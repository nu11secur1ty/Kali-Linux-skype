# Kali-Linux-skype
- Version 1.1
```bash
PRETTY_NAME="Kali GNU/Linux Rolling"
NAME="Kali GNU/Linux"
ID=kali
VERSION="2020.1"
VERSION_ID="2020.1"
VERSION_CODENAME="kali-rolling"
ID_LIKE=debian
ANSI_COLOR="1;31"
HOME_URL="https://www.kali.org/"
SUPPORT_URL="https://forums.kali.org/"
BUG_REPORT_URL="https://bugs.kali.org/"
```

[![](https://github.com/nu11secur1ty/Kali-Linux-skype/blob/master/logo/Skype-For-Linux-Logo.png)](https://youtu.be/XqFo3xKn46U)

# Direct install
```bash
curl -s https://raw.githubusercontent.com/nu11secur1ty/Kali-Linux-skype/master/AbeMasakatsu.sh | bash
```
Issue: 
```xml
This is the last root support of Skype
Running as root without --no-sandbox is not supported.
```
- Read more: https://crbug.com/638180
